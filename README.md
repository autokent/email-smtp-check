# email-smtp-check
> **Checks existence of E-Mail SMTP server.**

![logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/4802395/email-smtp-check.png)

[![version](https://img.shields.io/npm/v/email-smtp-check.svg)](https://www.npmjs.org/package/email-smtp-check)
[![downloads](https://img.shields.io/npm/dt/email-smtp-check.svg)](https://www.npmjs.org/package/email-smtp-check)
[![node](https://img.shields.io/node/v/email-smtp-check.svg)](https://nodejs.org/)
[![status](https://gitlab.com/autokent/email-smtp-check/badges/master/pipeline.svg)](https://gitlab.com/autokent/email-smtp-check/pipelines)

## Installation
`npm install email-smtp-check`


## Test
`mocha` or `npm test`

> check test folder and QUICKSTART.js for extra usage.